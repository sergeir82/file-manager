/**
 * Created by ratnikov on 8/24/14.
 */

var server = sinon.fakeServer.create();

server.autoRespond = true;
server.autoRespondAfter = 1000;

server.respondWith("/HTM_AJAX_HANDLER", function(xhr, id){
    fakeData(xhr);
});

server.respondWith("/CMD_FILE_MANAGER", function(xhr, id){
    console.log(xhr);
});

function fakeData(xhr){
    console.log(xhr);
    var i;
    var requestBody = xhr.requestBody;
    var requestParams = _.object(
        requestBody.split('&').map(
            function(x){
                return [x.split('=')[0], decodeURIComponent(x.split('=')[1])]
            })
    );
    var response ;

    for(i = 0; i < requests.length; i++){
        if(requestParams['q'].match(requests[i]['q']) && requestParams['rest'] == requests[i]['rest']){
            response = requests[i].do(xhr, requestParams);
            break;
        }
    }
    console.log('Req: ' + requestParams['rest'] + ' *** ' + requestParams['q']);
    console.log('Res: ' + xhr.responseText);

    if(response){
        xhr.respond(200, { 'Content-Type': 'application/json' }, response);
    }

}

function generateFileList(path, dirOnly){
    var initFileNum = 6;
    var initImageNum = 4;
    var initDirNum = 8;
    var arPath = path.split('/');
    var result = [];
    var i;
    if(!dirOnly) {
        for(i = 0; i < initFileNum - (arPath.length - 1); i++){
            result.push({"name": "file_" + (arPath.length - 1) + '_' + i + '.txt', "type": "file", "size": Math.floor(500 * (2 - Math.cos(i+1))), ctime: 1408018648, mtime: 1408018648, perm: '766', with_subdirs: false });
        }
        for(i = 0; i < initImageNum - (arPath.length - 1); i++){
            result.push({"name": "file_" + (arPath.length - 1) + '_' + i + '.jpg', "type": "file", "size": Math.floor(500 * (2 - Math.cos(i+1))), ctime: 1408018648, mtime: 1408018648, perm: '766', with_subdirs: false });
        }

    }

    for(i = 0; i < initDirNum - (arPath.length - 1); i++){
        result.push({"name": "dir_" + (arPath.length - 1) + '_' + i , "type": "dir", "size": "1", ctime: 1408018648, mtime: 1408018648, perm: '766' , with_subdirs: !!(initDirNum - arPath.length)});
    }
    return result;
}

function generateFileTree(dir, text, image, arc, other){


    var init = { dir: dir, text: text, image: image, arc: arc, other: other }
//    for(i = 0; i < initFileNum - (arPath.length - 1); i++){
//        result.push({"name": "file_" + (arPath.length - 1) + '_' + i + '.txt', "type": "file", "size": Math.floor(500 * (2 - Math.cos(i+1))), ctime: 1408018648, mtime: 1408018648, perm: '766', with_subdirs: false });
//    }
//    for(i = 0; i < initImageNum - (arPath.length - 1); i++){
//        result.push({"name": "file_" + (arPath.length - 1) + '_' + i + '.jpg', "type": "file", "size": Math.floor(500 * (2 - Math.cos(i+1))), ctime: 1408018648, mtime: 1408018648, perm: '766', with_subdirs: false });
//    }

    function _generateFile(cur, init, ext){
        var i, result = [];

        for(i = 0; i < cur ; i++){
            result.push(
                {
                    "name": "file_" + (init - cur) + '_' + i + '.' + ext,
                    "type": "file",
                    "size": Math.floor(500 * (2 - Math.cos(i+1))),
                    ctime: 1408018648,
                    mtime: 1408018648,
                    perm: '766'
                }
            );
        }
        return result;
    }

    function _generateFileTree(current, init){
        var result = [];
        var i, j, next;
        for(i = 0; i < current.dir ; i++){
            next = {};
            for(j in current){
                next[j] = Math.max(current[j] - i - 1, 0);
            }

            result.push(
                {
                    "name": "dir_" + (init.dir  - current.dir) + '_' + i ,
                    "type": "dir",
                    "size": "1",
                    ctime: 1408018648,
                    mtime: 1408018648,
                    perm: '766' ,
                    below: _generateFileTree(next, init)
                }
            );
        }

        return result
            .concat(_generateFile(current.text , init.text, 'txt'))
            .concat(_generateFile(current.arc , init.arc, 'zip'))
            .concat(_generateFile(current.image , init.image, 'jpg'))
            .concat(_generateFile(current.other , init.text, 'bin'));

    }

    return _generateFileTree(init, init);
}

var requests = [
    //Получение бекапов по датам
    {
        q: /files\/list.*?$/ ,
        rest: 'GET',
        do: function(xhr, p){


            xhr.respond(
                200,
                { 'Content-Type': 'text/html' },
                JSON.stringify(generateFileList(p.q.replace(/\/?\?.*$/, '').replace(/\/$/, '').replace('files/list', ''), !p.q.match(/type=.*?file/)))
//                '[{"name": "config.json", "type": "file", "size": "158", "mtime": "2013-08-23 13:38:00" },{"name": "cache/", "type": "dir", "size": "25", "mtime": "2013-08-23 12:07:00" },{"name": ".htaccess", "type": "file", "size": "13", "mtime": "2013-08-23 12:06:00" }]'
            );
        }

    },
    {
        q: /files\/move.*?$/ ,
        rest: 'PUT',
        do: function(xhr, p){
            xhr.respond(
                200,
                { 'Content-Type': 'text/html'  },
                ''
            );
        }
    },
    {
        q: /files\/edit.*?$/ ,
        rest: 'GET',
        do: function(xhr, p){
            xhr.respond(
                200,
                { 'Content-Type': 'text/html'  },
                JSON.stringify({
                    "encoding": 'utf-8', // Кодировка
                    "path": '/test/test/file.py',
                    "data": 'alert(123);',
                    "error": ''
                })
            );
        }
    },
    {
        q: /files\/copy/,
        rest: 'POST',
        do: function(xhr, p){

            var now = (new Date).getTime() / 1000;

            // Шаблон
            var job = {
                "job_queue_id": "eeebaa57-3347-11e4-b450-52540049b318",
                "job_sequence": {
                    "0": {
                        "state_id": 20,
                        "state_name": "vh_file_copy_queued"
                    },
                    "1": {
                        "state_id": 21,
                        "state_name": "vh_file_copy_start"
                    },
                    "2": {
                        "state_id": 22,
                        "state_name": "vh_file_copy_done"
                    }
                },
                "job_type": "vh_file_copy",
                "params": {},

                time: now
            };

            simpleStorage.set('filemanager.job', job);
            xhr.respond(
                200,
                ''
            );
        }
    },
    {
        q: /job\/[^\/]+?/ ,
        rest: 'GET',
        do: function(xhr, p){
            var job = simpleStorage.get('filemanager.job');
            var now = (new Date).getTime() / 1000;
            var time = now - parseInt(job['time']);
            if(time > 10){
                job['taken'] = 1;
                if(job['job_type'] == 'vh_file_copy'){
                    job['jts_id'] = Math.min(20 + Math.floor( time / 5), 22);
                }
            }

            var res = JSON.stringify({
                'jts_id': job['jts_id'],
                'taken': job['taken'],
                'error': job['error']
            });

            simpleStorage.set('filemanager.job', job);

            xhr.respond(
                200,
                { 'Content-Type': 'text/html' },
                res
            );

        }
    },
    {
        q: /job\/[^\/]+?/ ,
        rest: 'DELETE',
        do: function(xhr, p){
            var job = simpleStorage.get('backup.job');
            simpleStorage.deleteKey('filemanager.job');
            xhr.respond(
                200,
                { 'Content-Type': 'text/html' },
                '[]'
            );

        }
    },
    {
        q: /job(\?|$)/ ,
        rest: 'GET',
        do: function(xhr, p){

            var res = '[]';
            var job = simpleStorage.get('filemanager.job');
            if(job) {
                res = JSON.stringify([{
                    'job_queue_id': job['job_queue_id'],
                    'job_sequence' : job['job_sequence'],
                    'job_type' : job['job_type']
                }]
                )

            }
            xhr.respond(
                200,
                { 'Content-Type': 'application/json'},
                res
            );
        }
    }
]