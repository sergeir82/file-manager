/**
 *
 * @file Файловый менеджер
 * @author S.Ratnikov sergeir82@gmail.com
 *
 */

'use strict';
(function($){

    var ajaxUrl = '/HTM_AJAX_HANDLER';
    var imageUrl = '/CMD_FILE_MANAGER';

    var fileFormats = {
        'image': /^(?:gif|jpe?g|png|bmp)$/i,
        'text': /^(?:abap|as|ada|adb|^htaccess|^htgroups|^htpasswd|^conf|htaccess|htgroups|htpasswd|asciidoc|asm|ahk|bat|cmd|c9search_results|cpp|c|cc|cxx|h|hh|hpp|cirru|cr|clj|cljs|CBL|COB|coffee|cf|cson|^Cakefile|cfm|cs|css|curly|d|di|dart|diff|patch|^Dockerfile|dot|e|erl|hrl|ejs|frt|fs|ldr|ftl|gcode|feature|^.gitignore|glsl|frag|vert|go|groovy|haml|hbs|handlebars|tpl|mustache|hs|hx|html|htm|xhtml|erb|rhtml|html.erb|ini|conf|cfg|prefs|jack|jade|java|js|jsm|json|jq|jsp|jsx|jl|tex|latex|ltx|bib|less|liquid|lisp|ls|logic|lql|lsl|lua|lp|lucene|^Makefile|^GNUmakefile|^makefile|^OCamlMakefile|make|matlab|md|markdown|mel|mysql|mc|mush|nix|m|mm|ml|mli|pas|p|pl|pm|pgsql|php|phtml|ps1|praat|praatscript|psc|proc|plg|prolog|properties|proto|py|r|Rd|Rhtml|rb|ru|gemspec|rake|^Guardfile|^Rakefile|^Gemfile|rs|sass|scad|scala|smarty|tpl|scm|rkt|scss|sh|bash|^.bashrc|sjs|space|snippets|soy|sql|styl|stylus|svg|tcl|tex|txt|textile|toml|twig|ts|typescript|str|vala|vbs|vb|vm|v|vh|sv|svh|xml|rdf|rss|wsdl|xslt|atom|mathml|mml|xul|xbl|xq|yaml|yml)$/,
        'arc': /^(?:zip|rar|tar|gz)$/i
    }

    /**
     * Вспомогательные функции
     * @type {{dateFormat: dateFormat, sizeFormat: sizeFormat}}
     */
    var fileHelper = {
        /**
         * Отображение дат
         * @param ts  // Unix время в секундах
         * @returns {string}
         */
        dateFormat: function(ts){
            var date = new Date(ts * 1000);
            var norm = function(n){
                return n < 10 ? '0' + String(n) : String(n);
            }
            return norm(date.getDate()) + '.' + norm(+date.getMonth() + 1) + '.' +  date.getFullYear() + ' ' + norm(date.getHours()) + ':' + norm(date.getMinutes()) + ':' + norm(date.getSeconds());
        },
        /**
         * Человекочитаемые размеры файлов
         * @param bytes
         * @returns {*}
         */
        sizeFormat: function(bytes){
            var thresh = 1024;
            if(bytes < thresh) return bytes;
            var units = ['k','M','G','T','P','E','Z','Y'];
            var u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while(bytes >= thresh);
            return bytes.toFixed(1).replace(/\.0$/, '')+units[u];
        }
    }

    /**
     * Переменные для проекта
     * @type {{maxFileEditSize: number}}
     */
    var commonVars = {
        maxFileEditSize: 2000000,     // Максимальный размер редактируемого файла
        jobHideTimeout: 2000,          // После скольки микросекунд скрывать сообщение об выполненой работе
        jobHideTime: 500,             // Время скрытия вьюхи джоба
        jobRefreshTime: 2000,            // Период обновления статуса джоба
        docRootPath: '/domains/%s/public_html'       // Путь к папке, в которой лежат папки сайтов
    }

///////////////////////////
//  Модели и коллекции   //
///////////////////////////

    var FileModel = Backbone.Model.extend({
        defaults: {
            checked: false,
            isParent: false
        }
    });

    /**
     * Модель файла для editor
     * @type {*}
     */
    var FileEditModel = Backbone.Model.extend({
        defaults: {
            "encoding": 'utf-8', // Кодировка
            "path": '', // Путь к файлу
            "data": '', // Содержимое
            "error": '', // Ошибка при открытии
            "curPath": '' // Текущая папка, нужна для создания новых файлов
        }
    });

    // Узел в файловом дереве
    var FileTreeNode = Backbone.Model.extend({
            defaults:{
                fullPath: '',
                isOpen: false, // Открыта ли папка?
                loaded: false,   // Загружено ли содержимое папки?
                with_subdirs: true
            },

            loadNode: function(path){
                this.nodes = new FileCollectionTree();
                this.nodes.list(1, 0 , path);
                this.listenTo(this.nodes, 'reset',
                    function(){
                        this.nodes.each(function(model){
                            model.set({
                                'fullPath': this.get('fullPath') + '/' + model.get('name')
                            });
                        }.bind(this));
                        this.trigger('tree:file:loaded');
                    }

                )


            }
        }
    );

    // Модель работы
    var JobModel = Backbone.Model.extend({
        defaults: {
            error: false,
            cancel: false,
            result: { result: { completed: 0, errors: [] }}
        }
    });

    /**
     * Модель прав
     * @type {*}
     */
    var FilePermModel = Backbone.Model.extend({
        /**
         * Флажки прав
         */
        defaults: {
            ux: 0,  // Пользователь исполнение
            uw: 0,  // Пользователь запись
            ur: 0,  // Пользователь чтение
            gx: 0,  // Группа исполнение
            gw: 0,  // Группа запись
            gr: 0,  // Группа чтение
            ox: 0,  // Другие исполнение
            ow: 0,  // Другие запись
            or: 0   // Другие чтение
        },
        initialize: function(options){

            this.setCheckboxes(options.perm);

            // Событие изменения числовых прав
            this.on('change:perm', function(){
                this.setCheckboxes(this.get('perm'));
                this.trigger('file:perm:refresh');  // Посылаем событие для перерисовки чекбоксов
            }.bind(this));

            // Событие обновления числовых прав по флажкам
            this.on('refresh:num:perm', function(){
                this.set({'perm': [
                    + this.get('ux') + 2 * this.get('uw') + 4 * this.get('ur'),
                    + this.get('gx') + 2 * this.get('gw') + 4 * this.get('gr'),
                    + this.get('ox') + 2 * this.get('ow') + 4 * this.get('or')
                ].join('')}, {silent: true});       // Сетим perm без события change:perm
                this.trigger('change:num:perm', this.get('perm'));  // Посылаем событие для перерисовки числовых прав
            });
        },
        /**
         * Разбиваем числовые права на флажки
         * @param filePerm
         */
        setCheckboxes: function(filePerm){
            var perm, p;
            if(filePerm && filePerm.length == 3){
                perm = filePerm.split('');
                _.each(['u', 'g', 'o'], function(type){
                    p = +perm.shift();
                    _.each(['x', 'w', 'r'], function(action){
                        this.set(String(type) + String(action), p % 2);
                        p = (p - (p % 2)) / 2;
                    }.bind(this));
                }.bind(this));
            }
        }
    });

    var DocRootsModel = Backbone.Model.extend({
        defaults: {
            active: false
        }
    });

    /**
     * Модель хлебной крошки пути
     * @type {*}
     */
    var PathModel = Backbone.Model.extend({
        defaults: {
            first: false,
            last: false
        }
    });


    /**
     * Коллекция пути
     * @type {*|void}
     */
    var PathCollection = Backbone.Collection.extend('commonVars').extend({
        model: PathModel,
        list: function(path){
            path = path.replace(/^\/|\/$/g, '');                    // Обрубаем конечный и начальный слеш
            var home = this.getHomePath().replace(/^\/|\/$/g, '');  // Обрубаем конечный и начальный слеш
            var acc = [];
            var col = path.split('/').map(function(i){
                acc.push(i);
                return {
                    dir: i,
                    path: acc.join('/')
                }
            }.bind(this));

            if(!col[0]['dir']){                 // Добавляем 'домик' в путь
                col[0]['dir'] = home;
            }else{
                col.unshift({'dir': home, path: ''});
            }

            col[0]['first'] = true;             // Первая крошка (домик)
            col[col.length - 1]['last'] = true; // Последняя
            this.reset(col);                    // Обновляем коллекцию
        },
        /**
         * Получение 'домика' пользователя
         * @returns {string}
         */
        getHomePath: function(){
            return String($('.js-user-const').data('home'));
        }
    });

    var DocRootsCollection = Backbone.Collection.extend('commonVars').extend({
        model: DocRootsModel,
        list: function(){
            this.reset($('.js-user-const').data('sites'));
        },
        /**
         * Делаем модель с name == site активной
         * @param site имя сайта
         */
        setActive: function(site){
            var activeModel = this.where({active: true})[0];    // Предыдущая активная модель
            var model = this.where({name: site})[0];            // Ищем модель соответствующую свойству site
            if(activeModel == model){
                return;             // Оставляем как есть
            } else{
                activeModel && activeModel.set('active', false);    // Снимаем текущую активную отметку, если она есть
                model && model.set('active', true);                 // Ставим активную отметку если такой сайт есть.
            }
        }
    });

    /**
     * Абстрактный фаил коллекции
     * @type {*}
     */
    var FileCollectionAbstract = Backbone.Collection.extend({
        /**
         * Запрос на сервер
         * @param string url            // url к api, например /files/list
         * @param string rest           // POST, GET, PUT, DELETE
         * @param string data           // данные для запроса в виде JSON строки
         * @param function callback     // функция, вызываемая при успешном выполнении запроса
         * @param function fail         // функция, вызываемая при неудачном выполнении запроса
         * @param boolian isJson        // Должен ли вернуться json
         */
        ajax: function(url, rest, data, callback, fail, isJson){
            // Небольшая магия с замыканием во избежание перехлеста запросов
            (function(url, rest, data, callback, fail, isJson){
                var ajaxParams = {
                    url: ajaxUrl,
                    type: "POST",
                    data: {
                        'q': url,
                        'rest': rest,
                        'action': 'fileManager',
                        'data': data
                    }
                };

                if(isJson) {
                    ajaxParams['dataType'] = 'json';
                }

                $.ajax(ajaxParams).done(function(result){
                    _.isFunction(callback) && callback(result);
                }).fail(function(){
                    _.isFunction(fail) && fail();
                });
            })(url, rest, data, callback, fail, isJson);
        }
    });

    /**
     * Коллекция(модель) файлов
     * @todo переделать так, чтобы путь был без начального слеша (this.currentPath)
     * @type {*|void}
     */
    var FileCollection = FileCollectionAbstract.extend(commonVars).extend({
        model: FileModel,
        initialize: function(){
            this.oldPath = this.currentPath = '/';
        },

        // @todo это костыли и надо переделать
        setDir: function(dir){

            this.oldPath = this.currentPath;
            if(dir != '..'){
                this.currentPath = this.currentPath.replace(/\/+$/gm,'')  + '/' + dir
            } else {
                this.currentPath = this.currentPath.replace(/\/[^\/]+?$/, '');
                this.currentPath = this.currentPath ? this.currentPath : '/';
            }
            this.checkInSiteDir();
        },
        /**
         * Установка текущей директории
         * @param path
         */
        setPath: function(path){
            // @todo убрать когда приведем в порядок пути
            if(!path.match(/^\//)){
                path = '/' + path;
            }
            this.oldPath = this.currentPath;
            this.currentPath = path;
            this.checkInSiteDir();
        },
        // Получаем все выбранные имена файлов
        getCheckedNames: function(){
            return this.where({checked: true}).map(function(model){
                return model.get('name');
            });
        },

        /**
         * Получаем путь к избражению для просмотра
         * @param name // Имя файла
         * @returns {string}
         */
        getPathForImage: function(name){
            var path = (this.currentPath == '/') ? '/' : this.currentPath + '/';
            return imageUrl + path + name; // Это путь по которому мы будем запрашивать фаил
        },

        /**
         * Получение текущого файлового пути
         * @returns {string}
         */
        getCurrntPath: function(){
            return this.currentPath == '/' ? '/' : this.currentPath + '/';
        },

        /**
         * Нужно ли показывать родительский каталог
         * @returns {boolean}
         */
        isShowParent: function(){
            return this.currentPath != '/';
        },

        /**
         * Обновление списков файлов
         * @param showDir
         * @param showFiles
         */
        refresh: function(showDir, showFiles){
            this.list(showDir, showFiles);
        },
        /**
         * Проверка: находимся ли мы в директории сайта
         */
        checkInSiteDir: function(){
            var regStr = '^' + this.docRootPath.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&").replace('%s', '([^\/]+)');
            var reg = RegExp(regStr);
            var matches = this.currentPath.match(reg);
            if(_.isArray(matches)){
                this.trigger('file:in:site:dir', matches[1]);   // Если находимся
            } else {
                this.trigger('file:in:site:dir', '');           // Если нет
            }
        },

        /**
         * Получение списков файлов с сервера
         * @param showDir   // если 1, то показывать директории
         * @param showFiles // если 1, то показывать файлы
         * @param path      // Путь к директории, которую надо показать. Если не задано, то берется из currentPath
         */
        list: function(showDir, showFiles, path) {
            !showDir && !showFiles && (showDir = showFiles = 1 ) ;
            var showItems = [];
            showDir && showItems.push('dir');
            showFiles && showItems.push('file');
            this.trigger('file:list:before');               // Событие перед началом загрузки
            this.ajax(
                '/files/list' + (path ? path : this.currentPath) + '?type=' + showItems.join(','),
                'GET',
                {},
                function(result){
                    this.trigger('file:list:success');      // Событие успешного окончания загрузки
                    this.reset(result);
                }.bind(this),
                function(){
                    this.currentPath = this.oldPath ;
                    this.trigger('file:list:fail');         // Событие нудачного окончания загрузки
                }.bind(this),
                true
            )
        },

        /**
         * Копирование файлов в dest
         * @param dest
         * @param all // Копировать все
         */
        copy: function(dest, all){
            all && (all = false); // Копировать все
            var names, src, srcNames;
            if(all) {
                names = this.toJSON().map(function(model){
                    return model.get('name');
                });
            } else {
                names = this.getCheckedNames();
            }
            src = this.getCurrntPath();

            srcNames = names.map(function(name){
                return src + name;
            }.bind(this));

            this.ajax(
                '/files/copy' + (dest ? dest : '/'), // У корня должно быть /
                'POST',
                JSON.stringify({
                    source: srcNames
                }),
                function(result){
                    this.trigger('file:copy:staged');
                    this.refresh(); // Обновляем текущую папку
                }.bind(this),
                function(){
                    this.refresh(); //  Тоже обновляем
                    this.trigger('file:copy:fail');

                }.bind(this)
            );
        },

        /**
         * Перемещение файлов в dest
         * @param dest
         * @param all
         */
        move: function(dest, all){
            all && (all = false); // Перемещать всё
            var names, src, srcNames;
            if(all) {
                names = this.toJSON().map(function(model){
                    return model.get('name');
                });
            } else {
                names = this.getCheckedNames();
            }
            src = this.getCurrntPath();

            srcNames = names.map(function(name){
                return src + name;
            }.bind(this));

            this.ajax(
                '/files/move' + (dest ? dest : '/'), // У корня должно быть /
                'PUT',
                JSON.stringify({
                    source: srcNames
                }),
                function(result){
                    this.refresh(); // Обновляем текущую папку
                    if(_.isArray(result.error) && result.error.length > 0) {
                        this.trigger('file:move:error', result.error);
                    }
                }.bind(this),
                function(){
                    this.refresh(); //  Тоже обновляем
                    this.trigger('file:move:fail');

                }.bind(this),
                true
            );

        },

        /**
         * Переименование файла
         * Почти тоже самое, что и move, только с одним файлом
         * @param name  // Новое имя файла
         */
        rename: function(name){
            var oldName = this.getCheckedNames()[0];  // У нас переименуется только один файл!
            var src = this.getCurrntPath();
            var srcNames = [src + oldName];
            var targetName = [src + name];

            this.ajax(
                '/files/move' + targetName,
                'PUT',
                JSON.stringify({
                    source: srcNames
                }),
                function(result){
                    this.refresh(); // Обновляем текущую папку
                    if(_.isArray(result.error) && result.error.length > 0) {
                        this.trigger('file:rename:error', result.error);
                    }
                }.bind(this),
                function(){
                    this.refresh(); //  Тоже обновляем
                    this.trigger('file:rename:fail');

                }.bind(this),
                true
            );
        },

        delete: function(all){
            all && (all = false); // Удаляем всё
            var names, target, targetNames;
            if(all) {
                names = this.toJSON().map(function(model){
                    return model.get('name');
                });
            } else {
                names = this.getCheckedNames();
            }
            target = this.getCurrntPath();

            targetNames = names.map(function(name){
                return target + name;
            }.bind(this));

            this.trigger('file:delete:before');               // Событие перед началом удаления

            this.ajax(
                '/files/delete/',
                'DELETE',
                JSON.stringify({
                    target: targetNames
                }),
                function(result){
                    this.refresh(); // Обновляем текущую папку
                    if(_.isArray(result.error) && result.error.length > 0) {
                        this.trigger('file:delete:error', result.error);
                    }
                }.bind(this),
                function(){
                    this.refresh(); // Обновляем текущую папку
                    this.trigger('file:delete:fail');
                }.bind(this),
                true
            );

        },

        /**
         * Изменение прав на файлы
         * @param string perm  // Права в виде 775
         * @param boolian rec   // Рекурсивно?
         */
        changePerm: function(perm, rec){

            var names, target, targetNames;
            names = this.getCheckedNames();
            target = this.getCurrntPath();

            targetNames = names.map(function(name){
                return target + name;
            }.bind(this));

            this.ajax(
                '/files/chmod/',
                'PUT',
                JSON.stringify({
                    target: targetNames,
                    permissions: perm,
                    recursion: rec
                }),
                function(result){
                    this.refresh(); // Обновляем текущую папку
                    if(_.isArray(result.error) && result.error.length > 0) {
                        this.trigger('file:perm:error', result.error);
                    }
                }.bind(this),
                function(){
                    this.trigger('file:perm:fail');
                }.bind(this),
                true
            );
        },

        /**
         * Просмотр для изменения текстового файла с именем model.get('name')
         * @param model
         */
        viewText: function(model, encoding){
            var targetPath = this.getCurrntPath() + model.get('name');
            this.ajax(
                '/files/edit' + targetPath + (encoding ? '?encoding=' + encoding : ''),
                'GET',
                '',
                function(result){
                    this.trigger('file:edit:ready', new FileEditModel(result));     // Показываем редактор
                }.bind(this),
                function(){
                    this.trigger('file:view:text:fail');
                }.bind(this),
                true
            );
        },

        /**
         * Сохранение текстового файла
         * @param FileEditModel fileEditModel
         */
        saveText: function(fileEditModel){
            this.ajax(
                '/files/edit/' + fileEditModel.get('path'),
                'PUT',
                JSON.stringify({
                    encoding: fileEditModel.get('encoding'),
                    data: fileEditModel.get('data')
                }),
                function(result){
                    this.trigger('file:saved');
                    fileEditModel.trigger('file:saved'); // @todo Нужно чтобы фаил модель сама общалась с сервером
                    this.refresh();     // Размер файла, скорее всего, изменился. Обновляем коллекцию.
                }.bind(this),
                function(){
                    this.trigger('file:save:text:fail');
                }.bind(this)
            );
        },

        /**
         * Создание нового файла/директории в текущей директории
         * @param name
         * @param type  // file/dir
         */
        createFile: function(name, type){
            var curPath = this.getCurrntPath();
            this.ajax(
                '/files/make' + curPath + name,
                'PUT',
                JSON.stringify({
                    'type': type
                }),
                function(result){
                    this.refresh();                      //  Обновляем коллекцию.
                }.bind(this),
                function(){
                    this.trigger('file:create:fail');
                }.bind(this)
            );
        },

        /**
         * Сортировка коллекции файлов
         * @param a
         * @param b
         * @returns {number}
         */
        comparator: function(a, b) {
            var aType = a.get('type'), bType = b.get('type');
            var aName = a.get('name'), bName = b.get('name');
            if(aType == 'dir' && bType != 'dir'){
                return -1;
            } else if(aType != 'dir' && bType == 'dir') {
                return 1;
            }
            return aName == bName ? 0 : (aName < bName ? -1 : 1);
        }

    });

    /**
     * Коллекция всех джобов
     * @type {*|void}
     */
    var JobCollection = FileCollectionAbstract.extend({
        model: JobModel,
        /**
         * Получение джобов
         */
        list: function(){
            this.ajax(
                '/job?job_type=vh_file_copy',
                'GET',
                null,
                function(result){
                    this.trigger('job:list:loaded', result.length);     // Отправляем событие с числом джобов
                    result.splice(1);                                   // Мы работаем только с одним джобом
                    this.reset(result);                                 // Обновляем коллекцию джобов
                }.bind(this),
                function(){
                    this.trigger('job:list:fail');
                }.bind(this),
                true
            )
        },
        /**
         * Удаление
         * @param model
         */
        delete: function(model){
            this.ajax(
                '/job/' + model.get('job_queue_id'),
                'DELETE',
                {},
                function(result){
                    this.trigger('job:deleted')
                }.bind(this),
                function(){
                    this.trigger('job:delete:fail');
                }.bind(this)
            )
        },

        /**
         * Отмена
         * @param model
         */
        cancel: function(model){
            this.ajax(
                '/job/' + model.get('job_queue_id'),
                'PUT',
                JSON.stringify({
                    "cancel": true
                }),
                function(result){
                    this.trigger('job:canceled')
                }.bind(this),
                function(){
                    this.trigger('job:cancel:fail');
                }.bind(this)
            )
        },

        /**
         * Обновление
         */
        refresh: function(){
            this.list();
        },

        /**
         *  Получение статуса по джобам
         */
        getStatus: function(){
            var model = this.at(0); // У нас в коллекции только один джоб
            this.ajax(
                '/job/' + model.get('job_queue_id'),
                'GET',
                null,
                function(result){
                    // Костыль
                    if(_.isNull(result.result)){
                        delete result.result;
                    }
                    // Проставляем полученные поля в модель
                    // Если у нас будет несколько прогрессбаров, нужно предусмотреть, чтобы у нас были корректные переменные из замыкания
                    model.set(result);
                    this.calcStep(model);
                    this.trigger('job:status:loaded');
                }.bind(this),
                function(){
                    this.trigger('job:get:status:fail');
                }.bind(this),
                true
            );
        },

        /**
         * Вычисление шага
         * Первый шаг 0
         * @param model
         */
        calcStep: function(model){
            var step;
            var pairs = _.pairs(model.get('job_sequence'));
            if(model.get('taken')){
                var jts_id = model.get('jts_id');
                step = pairs.filter(function(i){
                    return i[1]['state_id'] == jts_id;
                })[0][0];
            } else {
                step = 0;   // Если работа не взята => шаг нулевой
            }

            model.set({
                'calcStep': parseInt(step),                 // Шаг
                'calcStepLength': parseInt(pairs.length)    // Общая длина шагов
            });
        }
    });

    /**
     * Коллекция дерева файлов
     * @type {*}
     */
    var FileCollectionTree = FileCollection.extend({
        model: FileTreeNode
    });


///////////////////////////
//  Отображения(view)    //
///////////////////////////

    /**
     * Вьюха с регионами таблицы файлов
     * @type {*}
     */
    var FileNavigationLayout = Backbone.Marionette.LayoutView.extend({
        regions: {
            head: ".js-file-manager-list-head",
            body: ".js-file-manager-list-body",
            jobs: ".js-file-jobs",
            block: ".js-file-manager-block",
            docroots: ".js-file-manager-docroots",
            path: ".js-file-manager-path"
        },
        template:'#file-manager-list-tpl',
        events:{
            'click .js-copy-files': 'copyFiles',
            'click .js-move-files': 'moveFiles',
            'click .js-upload-files': 'uploadFiles',
            'click .js-rename-file': 'renameFile',
            'click .js-delete-files': 'deleteFiles',
            'click .js-create-file': 'createFile',
            'click .js-create-dir': 'createDir',
            'click .js-perm-files': 'permFiles'
        },
        onShow: function(){

        },
        copyFiles: function(ev){
            this.trigger('copy:init');
        },
        moveFiles: function(ev){
            this.trigger('move:init');
        },
        renameFile: function(ev){
            this.trigger('rename:init');
        },
        uploadFiles: function(ev){
            this.trigger('upload:init');
        },
        deleteFiles: function(ev){
            this.trigger('delete:init');
        },
        createFile: function(ev){
            ev.preventDefault();
            this.trigger('create:file:init');
        },
        createDir: function(ev){
            ev.preventDefault();
            this.trigger('create:dir:init');
        },
        permFiles: function(ev){
            ev.preventDefault();
            this.trigger('perm:init');
        },
        /**
         * Делаем кнопку копирования неактивной если n > 0
         * @param n
         */
        disableCopy: function(n){
            if(n > 0){
                this.$el.find('.js-copy-files').button( "option", "disabled", true );
            } else {
                this.$el.find('.js-copy-files').button( "option", "disabled", false );
            }
        },
        onRender: function(){
            this.$el.find('button').button();
            this.$el.find('.js-file-manager-menu').menu();
            // Показываем подменю при ховере
            this.$el.find('.js-create-files').parent().hover(
                function(){
                    $(this).find('.js-file-manager-menu').show();
                }, function(){
                    $('.js-file-manager-menu').hide();
                })
        },
        /**
         * Показываем контекстное меню для ситуации, когда ни один файл не выбран
         */
        contextForEmpty: function(){
            this.hideContext(this.$el.find('.js-copy-files-wrap, .js-move-files-wrap, .js-rename-files-wrap, .js-delete-files-wrap, .js-perm-files-wrap'));
            this.showContext(this.$el.find('.js-create-file-wrap, .js-upload-files-wrap'));
        },
        /**
         * Показываем контекстное меню для ситуации, когда один файл выбран (не архив)
         */
        contextForOne: function(){
            this.hideContext(this.$el.find('.js-create-file-wrap, .js-upload-files-wrap'));
            this.showContext(this.$el.find('.js-copy-files-wrap, .js-move-files-wrap, .js-rename-files-wrap, .js-delete-files-wrap, .js-perm-files-wrap'));
        },
        /**
         * Выбрано много файлов
         */
        contextForMany: function(){
            this.hideContext(this.$el.find('.js-create-file-wrap, .js-rename-files-wrap, .js-upload-files-wrap'));
            this.showContext(this.$el.find('.js-copy-files-wrap, .js-move-files-wrap, .js-delete-files-wrap, .js-perm-files-wrap'));
        },
        /**
         * Выбран архив
         */
        contextForArc: function(){
            this.hideContext(this.$el.find('.js-create-file-wrap, .js-upload-files-wrap'));
            this.showContext(this.$el.find('.js-copy-files-wrap, .js-move-files-wrap, .js-rename-files-wrap, .js-delete-files-wrap, .js-perm-files-wrap'));
        },
        /**
         * Показать элементы контекстного меню
         * @param $el
         */
        hideContext: function($el){
            $el.hide();
        },
        /**
         * Скрыть элементы контекстного меню
         * @param $el
         */
        showContext: function($el){
            $el.fadeIn('slow');
        }
    });

    /**
     * Ячейка шапки таблицы
     * @type {*}
     */
    var FileNavigationHeadCellView = Backbone.Marionette.ItemView.extend({
        template: '#file-navigation-head-cell-tpl',
        tagName: 'td',
        initialize: function(){
            this.$el.addClass('col-' + this.model.get('field'));
            this.$el.addClass('listtitle'); // класс DA
        }
    });

    /**
     * Ячейка шапки таблицы с чекбоксом
     * @type {*|void}
     */
    var FileNavigationHeadCellCheckedView = FileNavigationHeadCellView.extend({
        template: '#file-navigation-head-cell-check-tpl',
        tagName: 'td'
    });

    /**
     * Шапка таблицы
     * @type {*}
     */
    var FileNavigationHeadView = Backbone.Marionette.CompositeView.extend({
        template: '#file-navigation-head-tpl',
        tagName: 'tr',
        childView: FileNavigationHeadCellView,
        childCheckedView: FileNavigationHeadCellCheckedView,
        initialize: function(options){
            // Создаем коллекцию с названиями столбцов в шапке
            this.collection = new Backbone.Collection(
                _.pairs(options['showFields']).map(function(pair){
                    return {field: pair[0], value: pair[1] };
                })
            )
        },
        /**
         * Для разных полей могут быть разные шаблоны
         * @param child // модель
         * @returns {*}
         */
        getChildView: function(child) {
            // @todo Дублирование !!!
            var func = this.getOption('child' + this.capitalise(child.get('field')) + 'View'); // Ищем шаблон ячейки для данного поля
            if(!_.isFunction(func)) {
                func = this.constructor.__super__.getChildView.apply(this, arguments); // Если не нашли берем childView
            }
            return func;
        },
        capitalise: function (string) { // @todo вынести в библиотеку
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }
    });

    /**
     * Ячейка с содержимым, к примеру именем файла, размером и т.д.
     * @type {*}
     */
    var FileNavigationCellView = Backbone.Marionette.ItemView.extend({
        template: '#file-navigation-cell-tpl',
        tagName: 'td',
        initialize: function(){
            this.$el.addClass('col-' + this.model.get('field'));
        },
        capitalise: function (string) { // @todo вынести в библиотеку
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }
    });

    /**
     * Ячейка checkbox в теле таблицы
     * @type {*|void}
     */
    var FileNavigationCellCheckedView = FileNavigationCellView.extend({
        template: '#file-navigation-cell-check-tpl',
        events: {
            'change input[type="checkbox"]': 'selectFile'
        },
        selectFile: function(ev){
            var checked = $(ev.target).is(':checked');              // Статус чекбокса
            this.model.set('value', checked);                       // Ставим текущей модели поле значения
            this.model.get('fileModel').set('checked', checked);    // Ставим модели файла поле выбора
        }
    });

    /**
     *  Ячейка имени со ссылкой в теле таблицы
     */
    var FileNavigationCellNameView = FileNavigationCellView.extend(commonVars).extend({
        template: '#file-navigation-cell-name-tpl',                     // Основной шаблон
        templateImage: '#file-navigation-cell-name-image-tpl',          // Шаблон для картинок
        events: {
            'click .js-open-dir': 'changePath',
            'click .js-open-file': 'viewFile'
        },
        fileFormats: _.isObject(fileFormats) ? fileFormats : {} , // Расширения файлов по типам
        initialize: function(){
            this.constructor.__super__.initialize.apply(this, arguments);
            var fileFormat, resultFileFormat;
            this.fileEditable = true;
            var type = this.model.get('fileModel').get('type');
            var ext = this.model.get('value').match(/\.([^.]+?$)/); // Расширение файла
            ext = _.isArray(ext) ? ext[1] : '';
            if(ext && type == 'file'){
                for (fileFormat in this.fileFormats) {
                    if(ext.match(this.fileFormats[fileFormat])){
                        if(this['template'+this.capitalise(fileFormat)]) {
                            this.template = this['template'+this.capitalise(fileFormat)]; // Устанавливаем этот шаблон если он есть
                        }
                        this.$el.addClass('ftype-' + type + '-' + fileFormat); // Устанавливаем класс с типа файла
                        resultFileFormat = fileFormat;                          // Запоминает тип файла
                        break;
                    }
                }
            }

            if(type != 'file' || (resultFileFormat && resultFileFormat != 'text')){     // Если расширение не текстовое то запрещаем редактирование в редакторе
                this.fileEditable = false;
            }

            this.$el.addClass('ftype-' + type);  // Здесь должна устанавливаться иконка файла

            if(this.model.get('value') == '..'){    //  Родительская папка
                this.$el.addClass('ftype-parent');
            }
        },

        /**
         * Переход по директориям
         * @param ev
         */
        changePath: function(ev){
            ev.preventDefault();
            ev.stopPropagation();   // Нужно чтобы событие дальше не распространялось! Может привести к ошибке механизма двойного нажатия!
            this.model.get('fileModel').trigger('change:path', this.model.get('fileModelJSON').name);
        },

        /**
         * Просмотр текстового файла
         * @param ev
         */
        viewFile: function(ev){
            ev.preventDefault();    // Нужно чтобы событие дальше не распространялось! Может привести к ошибке механизма двойного нажатия!
            ev.stopPropagation();
            if(this.fileEditable){  // Если фаил можно редактировать, открываем его
                if(this.model.get('fileModelJSON').size > this.maxFileEditSize){
                    // Фаил слишком большой для редактирования
                    this.trigger('file:big:size:error');
                } else {
                    this.trigger('file:open');
                }
            }
        }
    });

    /**
     * Ячейки с датами
     * @type {*|void}
     */
    var FileNavigationCellDateView = FileNavigationCellView.extend(fileHelper).extend({
        templateHelpers:function(){
            var value = this.options.model.get('value');
            return {
                value: _.isNumber(value) ? this.dateFormat(value) : ''
            }
        }
    });

    /**
     * Ячейка размера файла
     * @type {*}
     */
    var FileNavigationCellSizeView = FileNavigationCellView.extend(fileHelper).extend({
        templateHelpers:function(){
            var type = this.options.model.get('fileModelJSON').type;
            var value = this.options.model.get('value');
            return {
                value: _.isNumber(value) && type == 'file'  ? this.sizeFormat(value) : ''
            }
        }
    });

    /**
     * Ряд с информацией о файле
     */
    var FileNavigationRowView = Backbone.Marionette.CompositeView.extend({
        template: '#file-navigation-row-tpl',
        childView: FileNavigationCellView,                  // Обычная ячейка
        childCheckedView: FileNavigationCellCheckedView,    // Чекбокс
        childNameView: FileNavigationCellNameView,          // Имя файла
        childCtimeView: FileNavigationCellDateView,         // Дата
        childMtimeView: FileNavigationCellDateView,         // Дата
        childSizeView: FileNavigationCellSizeView,          // Размер
        tagName: 'tr',
        className: 'file-row',
        events: {
            'click': 'rowClick',
            'dblclick': 'rowDblClick'
        },
        initialize: function(options){
            var args = _.keys(options.showFields);      // Какие поля мы будем показывать
            args.unshift(this.model.toJSON())           // Как-то очень заумно
            var modelFields = _.pick.apply({}, args);   // Какие поля модели будем показывать

            this.collection = new Backbone.Collection(                  //  Создаем коллекцию для ячеек в ряду таблицы
                _.pairs(modelFields)
                    .map(
                        function(pair){
                            return {
                                field: pair[0],                         //  Имя поля
                                value: pair[1] ,                        //  Значение поля
                                fileModel: this.model,                  //  Ссылка на модель из коллекции файлов
                                fileModelJSON: this.model.toJSON()      //  Модель в виде JSON объекта
                            };
                        }.bind(this)
                    )
            );

            this.on('row:click', this.rowClick.bind(this));
            this.on('row:dblclick', this.rowDblClick.bind(this));
            this.on('set:checked', this.setChecked.bind(this));


            if(this.model.get('checked')) {     // Если фаил выбран
                this.$el.addClass('checked');
            }

            this.$el.addClass('slist2'); // класс DA
        },

        /**
         * Для разных полей могут быть разные шаблоны
         * @param child // модель
         * @returns {*}
         */
        getChildView: function(child) {
            // @todo Очень странно работает замыкание
            var func = this.getOption('child' + this.capitalise(child.get('field')) + 'View'); // Ищем шаблон ячейки для данного поля
            if(!_.isFunction(func)) {
                func = this.constructor.__super__.getChildView.apply(this, arguments); // Если не нашли берем childView: FileNavigationCellView,
            }
            return func;
        },


        /**
         * "Наш" одиночный клик
         */
        rowClick: function(ev){
            var shift = ev.shiftKey;
            var ctrl = ev.ctrlKey;
            if (this.model.get('name') == '..') return; // Ничего не делаем для родительской директории
            var checked = this.model.get('checked');
            this.model.set('checked', true);        // Меняем значение модели на обратное
            if(ctrl) {
                this.$el.addClass('checked');
                this.trigger('set:last');               // Этот ряд мы кликнули последним
            } else if(shift) {                          // Если нажат shift то выделяем все от предидущего до текущего, либо только элемент по которому кликнули
                this.trigger('set:shift');
            } else {
                this.trigger('set:one');                // Выбираем только его одного
                this.trigger('set:last');
            }
            this.trigger('file:row:clicked');           // Событие что на файл кликнули и надо переделать контекстное меню
        },

        /**
         * "Наш" двойной клик
         */
        rowDblClick: function(){
            this.$el.find('.js-action').trigger('click');   // Мы находим в ряду ссылку с классом js-action кликаем на ней @ todo переделать
        },
        /**
         * Проставляем отметку для данного вью
         * @param checked
         */
        setChecked: function(checked){
            if (this.model.get('name') == '..') return; // Ничего не делаем для родительской директории
            this.model.set('checked', checked);
            if (checked) {
                this.$el.addClass('checked');
            } else {
                this.$el.removeClass('checked');
            }
        },
        /**
         * Удаляем выделение текста к примеру при нажатом shift
         */
        clearSelection: function() {
            if(document.selection && document.selection.empty) {
                document.selection.empty();
            } else if(window.getSelection) {
                var sel = window.getSelection();
                sel.removeAllRanges();
            }
        },
        capitalise: function (string) { // @todo вынести в библиотеку
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }
    });

    /**
     * "Тело" таблицы содержимого файлового менеджера
     * @type {*}
     */
    var FileNavigationListView = Backbone.Marionette.CompositeView.extend({
        childView: FileNavigationRowView,
        tagName: "tbody",
        template: '#file-navigation-list-tpl',
        fileFormats: fileFormats,
        initialize: function(options){
            this.childViewOptions = _.pick(options, 'showFields');
            // Перехватываем событие открытия файла
            // Оно приходит от "внучатого" view
            this.collection.listenTo(
                this,
                'childview:childview:file:open',
                function(view){
                    this.collection.viewText(view.model);
                }.bind(this));

            // Событие установки последнего кликнутого ряда
            this.on('childview:set:last', function(childView){
                this.children.each(function(view){
                    view.last = (view == childView);    // Устанавливаем вью выбранную последней
                });
            }.bind(this));

            // Событие выделения всех элементов с прошедшего до текущего по shift
            this.on('childview:set:shift', function(childView){
                var views = [];             // Элементы для выделения
                var viewsNotChecked = [];   // Элементы для снятия выделения
                var start = false;          // Флаг начала выделения
                var stop = false;           // Флаг окончания выделения

                this.children.each(function(view){
                    if(view == childView && view.last){                                     // Текущий ивляется и предидущим
                        views.push(view);
                    } else {
                        if((view == childView || view.last) && !start){                     // Нам попался либо текущий, либо предидущий
                            views.push(view);
                            start = true;
                        } else if((view != childView && !view.last) && start && !stop){     // Есть флаг старта, стопа ещё нет
                            views.push(view);
                        } else if((view == childView || view.last) && start){               // Нам попался либо текущий, либо предидущий ещё раз => всё стоп
                            views.push(view);
                            stop = true;
                        } else {
                            viewsNotChecked.push(view);
                        }
                    }

                });

                if(start && !stop) {                        // Это случай когда на таблице еще ничего не выбрано, но мы жмем shift и кликаем
                    views.splice(1);
                    views[0].trigger('set:checked', true);  // Одного делаем активным
                    views[0].last = true;                   // Отметка предидущего клика
                } else {
                    _.each(views, function(view){
                        view.trigger('set:checked', true);  // Активные
                    });
                    _.each(viewsNotChecked, function(view){
                        view.trigger('set:checked', false); // Неактивные
                    });
                }

            }.bind(this));

            // Сбрасываем флаг для всех вью
            this.on('childview:remove:last', function(childView){
                this.children.each(function(view){
                    view.last = false;
                });
            });

            // Оставляем только один выделенный элемент
            this.on('childview:set:one', function(childView){
                this.children.each(function(view){
                    view.trigger('set:checked', childView == view);
                });
            });

            // Убираем выделение всего
            this.on('remove:selection', function(){
                this.children.each(function(view){
                    view.trigger('set:checked', false);
                    view.last = false;
                });
                this.trigger('file:empty:selected');
            });

            // Клик по ряду файла, мы изменяем контекст меню по тому что у нас выбрано
            this.on('childview:file:row:clicked', function(){
                var checkedModels = this.collection.where({checked: true});     //  Выбранные файлы
                var f, format = 'file';
                if(!checkedModels.length){                                      // Если ничего не выбрано
                    this.trigger('file:empty:selected');
                } else if(checkedModels.length == 1){
                    if(checkedModels[0].get('type') == 'file'){                 // Если выбран один файл
                        var ext = checkedModels[0].get('name').match(/\.([^.]+?$)/); // Расширение файла
                        ext = _.isArray(ext) ? ext[1] : '';
                        for(f in this.fileFormats){
                            if(ext.match(this.fileFormats[f])){
                                format = f;
                                break;
                            }
                        }
                        this.trigger('file:' + format + ':selected');           // Посылаем событие, что выбран архив, картинка, текст или просто file
                    } else {
                        this.trigger('file:' + checkedModels[0].get('type') + ':selected');     // Выбрана директория
                    }
                } else {
                    this.trigger('file:many:selected');                         // Выбрано несколько файлов
                }
            });


        },
        onShow: function(){
            var that = this;
            // Инициализация просмотра изображений
            this.$el.find('.js-image-group').on('click', function(ev){
                ev.preventDefault();
                ev.stopPropagation();                                   // Нужно чтобы событие дальше не распространялось!
                $.colorbox({
                    rel:'js-image-group',
                    top:'10%',
                    maxWidth:'90%',
                    maxHeight:'80%',
                    href: function(colorbox){
                        return that.collection.getPathForImage($(this).data('name'));
                    }.bind(this)
                });
            });

            // Немного магии с кликами в "молоко"
            // Еcли элемент не .js-click-in и не является потомком .js-click-in, то сбрасываем всё выделение
            $('body').on("click.fileman", function(ev){
                if(!$(ev.target).closest('.js-click-in').length){
                    this.trigger('remove:selection');
                }
            }.bind(this));
        },
        onBeforeDestroy: function(){
            $('body').off("click.fileman");     // При дестое вьюшки отвязываем функционал клика в "молоко"
        }
    });

    /**
     * Вьюшки дерева файлов
     * Реализовано как в топике http://lostechies.com/derickbailey/2012/04/05/composite-views-tree-structures-tables-and-more/
     * @type {*}
     */
    var FileTreeView = Backbone.Marionette.CompositeView.extend({
        template: "#file-tree-tpl",
        tagName: "ul",
        className: 'file-tree-node',

        initialize: function(){
            this.collection = this.model.nodes;
            this.on('before:render', function(){
                //  Проставляем класс в зависимости от глубины
                this.$el.addClass(this.className + '-' + (this.model.get('fullPath').split('/').length - 1));
                this.collection = this.model.nodes;
            }.bind(this));
        },

        appendHtml: function(collectionView, itemView){
            collectionView.$("li:first").append(itemView.el);
        },

        events:{
            'click .js-tree-open-node': 'showDir',
            'dblclick .js-tree-node': 'showDir',
            'click .js-tree-node': 'selectDir'
        },

        ui: {
            openNode: '.js-tree-open-node'
        },

        /**
         * Открытие директории
         * @param ev
         */
        showDir: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            // Чтобы не дергать сервер много раз при открытии закрытии папок,
            // мы при закрытии просто скрываем дочерние элементы
            // а при последующем нажатии делаем их видимыми.
            if(!this.model.get('loaded') && !this.model.get('isOpen')){                         // Содержимое папки не загружено и она закрыта
                this.model.loadNode(this.model.get('fullPath'));
                this.ui.openNode.removeClass('plus').addClass('minus');
                this.model.set('isOpen', true);
                this.model.set('loaded', true);
                this.listenToOnce(this.model, 'tree:file:loaded', this.listLoaded);
                this.$el.parents('.js-file-tree-view').next('.js-block-dialog-region').show();  // Показываем загрузчик @todo переделать на events & layout
            } else if(this.model.get('isOpen')) {                                               // Папка открыта
                this.ui.openNode.parents('.file-tree-node:first').find('> .file-tree-node').hide();
                this.model.set('isOpen', false);
                this.ui.openNode.removeClass('minus').addClass('plus');
            } else if(this.model.get('loaded') && !this.model.get('isOpen')){                   // Папка закрыта, но содержимое её известно
                this.ui.openNode.parents('.file-tree-node:first').find('> .file-tree-node').show();
                this.model.set('isOpen', true);
                this.ui.openNode.removeClass('plus').addClass('minus');
            }
        },

        /**
         * Выбираем директорию из дерева
         * @param ev
         */
        selectDir: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            // @todo переделать
            $(".file-tree-node .active").removeClass('active');
            $(ev.target).addClass('active');
        },
        listLoaded:function(){
            this.render();
            this.$el.parents('.js-file-tree-view').next('.js-block-dialog-region').hide();      // Скрываем загрузчик @todo переделать на events & layout
        }
    });

    /**
     * Вьюшка редактора файлов
     * @type {*|void}
     */
    var FileEditorView = Backbone.Marionette.ItemView.extend({
        template: "#js-file-manager-editor-tpl",
        events: {
            'success.form.bv .js-editor-form': 'save',
            'click .js-cancel': 'cancel'
        },
        initialize: function(options){
            this.listenTo(options.model, 'file:saved', function(){     // Слушаем, что модель сохранилась
                this.hideSaveLoader('Сохранено');
                if(this.closeAfterSave){
                    options.onCloseEditor();
                }


            }.bind(this));


        },
        onShow: function() {
            // Показываем ace editor
            var editor = this.editor = ace.edit(this.$el.find('.content').get(0));
            editor.setTheme("ace/theme/katzenmilch");                               // Цветовое оформление
            editor.setValue(this.model.get('data'));                                // Добавляем содержимое файла в редактор
            var modelist = ace.require('ace/ext/modelist');                         // Типы файлов
            var filePath = this.model.get('path');                                  // Путь к файлу
            var mode = modelist.getModeForPath(filePath).mode;
            editor.session.setMode(mode);
            editor.clearSelection();                                                // Почему-то редактор открывается с выделенным текстом
            editor.focus();                                                         // Ставим в него курсор

            // Добавляем jquery ui стиль кнопок
            this.$el.find('.js-save, .js-cancel').button();

            var error = this.model.get('error');
            if(error){
                editor.setReadOnly(true);       // Если ошибка чтения делаем редактор ридонли
                this.$el.find('.js-save').attr('disabled', 'disabled');
            }



            // Бутстраповская валидация формы
            // Когда мы редактируем уже существующий фаил поля имени нет и его мы не валидируем!
            this.$el.find('.js-editor-form').bootstrapValidator({
                fields: {
                    fileName: {
                        validators: {
                            notEmpty: {
                                message: 'Имя не может быть пустым!'
                            }
                        }
                    }
                }
            });

        },
        /**
         * Сохранение текста
         */
        save: function(ev){
            ev.preventDefault();
            this.closeAfterSave = true;         // Закрыть редактор после сохранения
            var path = this.model.get('path');
            // Если путь к файлу пустой => фаил новый
            if(!path){
                path = this.model.get('curPath') + $(ev.target.fileName).val();
                this.model.set('path', path);
            }
            this.model.set('data', this.editor.getValue());     // Вносим изменившейся текст в модель
            this.showSaveLoader('Сохранение');
            this.trigger('file:save:editor', this.model);       // Данную модель надо сохранить
        },
        /**
         * Показываем крутилку в окне редактора
         * @param text // описание действия
         */
        showSaveLoader: function(text){
            this.$el.find('.js-status').addClass('loader').text(text).show('fast');
        },
        /**
         * Убираем крутилку
         * @param text // результат операции
         */
        hideSaveLoader: function(text){
            this.$el.find('.js-status').removeClass('loader').text(text);
            setTimeout(function(){
                this.$el.find('.js-status').hide('slow');
            }.bind(this), 1000);
        },
        /**
         * Закрываем редактор
         * @param ev
         */
        cancel: function(ev){
            this.options.onCloseEditor();
        }

    });

    /**
     * Вьюшка джоба
     * @type {*}
     */
    var JobView = Backbone.Marionette.ItemView.extend(commonVars).extend({
        template: '#js-file-job-tpl',
        tagName: 'li',
        className: 'file-job',
        initialize: function(){
            this.prevStep = -1;                         // Это предыдущий шаг
            this.on('job:refresh', function(){
                // Мы ререндерим шаблон если меняется шаг
                // Если шаг не меняется и имеет номер 1, то мы просто обновляем прогресбар
                var step = this.model.get('calcStep');  // Это текущий шаг
                var result = this.model.get('result');
                var isError = this.model.get('error');
                var isCancel = this.model.get('cancel');
                if(step == 1 && this.prevStep == 1){    // 1 это прогрессбар
                    this.$el.find('.js-file-progressbar').progressbar( "option", "value", this.model.get('result')['result']['completed']);
                } else {
                    this.render();                      // перерисовка
                }
                // Если работа имеет флаг error и не имеет флаг cancel
                if(isError && !isCancel){
                    this.errorJob();
                    this.slowHide();
                    // Если мы дошли до конечного шага
                } else if(step == 2) {
                    this.slowHide();
                }
                this.prevStep = step;
            }.bind(this));
            this.trigger('job:refresh');    // Вызываем рефрешь . Фиксим багу с незакрывающимися сообщениями
        },
        /**
         * Для разных шагов разный шаблон джоба
         * @param model
         * @returns {*}
         */
        getTemplate: function(model){
            var step = this.model.get('calcStep');
            var isError = this.model.get('error');
            var template;
            if(step == 0 && !isError){                                  // Шаг ожидания
                template = '#js-file-job-wait-tpl';
            } else if(                                                  // Шаг окончания
                step + 1 == this.model.get('calcStepLength') ||         // Либо последний шаг
                    isError                                             // Либо отмена или ошибка
                ){
                template = '#js-file-job-end-tpl';
            } else if(step > 0 && step < this.model.get('calcStepLength') -1) {     // Шаги прогрессбара
                template = this.template;
            } else {
                template = _.template('');
            }
            return template;
        },
        onRender: function(){

            var step = this.model.get('calcStep');
            this.$el.find('.js-cancel-job').button();   // Делаем кнопку отмены красивой

            this.slowShow();    // Проверяем показываем ли мы область джобов, также как и в onShow

            // Если шаг 1 (копирование) обновляем прогрессбар
            if(step === 1){
                this.$el.find('.js-file-progressbar').progressbar({
                    value: this.model.get('result')['result']['completed']
                });
            }

        },
        onShow: function(){
            this.slowShow();    // Проверяем показываем ли мы область джобов, также как и в onRender
        },
        events:{
            'click .js-delete-job': 'deleteJob',
            'click .js-cancel-job': 'cancelJob',
            'click .js-copy-error': 'errorJob'
        },

        /**
         * Удаление джоба
         * @param ev
         */
        deleteJob: function(ev){
            ev.preventDefault();
            this.trigger('job:delete');
        },

        /**
         * Отмена джоба
         * @param ev
         */
        cancelJob: function(ev){
            ev.preventDefault();
            this.trigger('job:cancel');
        },
        /**
         * Медленно показываем область джобов, если эта облать с скрыта и в jts_id что-то есть
         * Области может и не быть
         */
        slowShow: function(){
            if(this.$el.parents('.file-jobs-wrap').is(':hidden') && this.model.get('jts_id')){
                this.$el.parents('.file-jobs-wrap').stop(true, true).slideDown(this.jobHideTime);
            }
        },
        /**
         * Медленно скрываем область джобов
         */
        slowHide: function(){
            setTimeout(function(){
                this.$el.parents('.file-jobs-wrap').stop(true, true).slideUp(this.jobHideTime);
            }.bind(this), this.jobHideTimeout);
        },
        /**
         * Ошибки в процессе выполнеия
         * @param ev
         */
        errorJob: function(){
            var result = this.model.get('result');
            // В результате есть непустые ошибки показываем окно ошибок.

            if(result.result.errors.length){
                this.model.trigger('file:copy:error', result.result.errors);
            }
        }

    });

    // Вьюшка у нас нет джобов
    var JobEmptyView = Backbone.Marionette.ItemView.extend({
        template: _.template('')    // Пустота
    });

    /**
     * Вьюшка листа джобов
     * @type {*}
     */
    var JobListView = Backbone.Marionette.CompositeView.extend({
        childView: JobView,
        childViewContainer: "ul",
        template: '#js-file-job-list-tpl',
        emptyView: JobEmptyView,
        initialize: function(){
            this.on('before:render:collection', function(){
                this.$el.removeClass('empty');
            });
            this.on('render:empty', function(){
                this.$el.addClass('empty');
            });

        }
    });


    // Вьюшка блокировки контента
    var BlockView = Backbone.Marionette.ItemView.extend({
        template: '#js-file-block-tpl',
        showBlock: function(){
            this.$el.find('.js-file-block').show();
        },
        hideBlock: function(){
            this.$el.find('.js-file-block').hide();
        }
    });

    /**
     * Вьюшка формы установки прав
     * @type {*}
     */
    var FilePermView = Backbone.Marionette.LayoutView.extend({
        template: '#js-file-perm-form-tpl',
        regions: {
            'permCheckboxes': '.js-perm-checkboxes'     // Регион чекбоксов
        },
        events: {
            'click .js-cancel': 'cancel',
            'keyup .js-perm-num': 'changePerm',          // Меняем числовое поле прав
            'success.form.bv form': 'savePerm'          // Сохраняем валидный набор прав
        },

        /**
         * Выходим из диалога
         * @param ev
         */
        cancel: function(ev){
            ev.preventDefault();
            this.trigger('file:perm:close');            // Кнопка отмены
        },
        onRender: function(){
            // jquery.inputmask и bootstrapvalidator немного конфликтуют, что решилось введением события maskedchange на проверяемом инпуте.
            this.$el.find('.js-perm-num').inputmask("Regex",{   // маска 3 числа от 0 до 7
                "onKeyUp": function(){
                    this.$el.find('.js-perm-num').trigger('maskedchange');  // Вызываем событие maskedchange на нашем инпуте
                }.bind(this)
            });   // Маска на поле числовых прав
            this.$el.find('.js-save, .js-cancel').button();
            this.$el.find('form').bootstrapValidator({
                fields: {
                    filePerm:{
                        trigger: 'maskedchange',                            // Валидируем инпут по событию maskedchange
                        validators:{
                            regexp: {
                                regexp: /^[0-7]{3}$/i,                      // 3 числа от 0 до 7
                                message: 'Недопустимый набор прав.'
                            },
                            notEmpty:{                                      // не пустое
                                message: 'Недопустимый набор прав.'
                            }
                        }
                    }
                }
            })
        },
        /**
         * Событие изменения числовых прав
         * @param ev
         */
        changePerm: function(ev){
            this.trigger('file:perm:num:change', $(ev.target).val());   // Числовые права изменились
        },
        /**
         * Установка значения числовых прав
         * @param perm
         */
        setNumPerm: function(perm){
            this.$el.find('.js-perm-num').val(perm).trigger('maskedchange');
        },
        /**
         * Сохраняем числовое значение прав
         * @param ev
         */
        savePerm: function(ev){
            ev.preventDefault();
            this.trigger(
                'file:perm:save',
                this.$el.find('.js-perm-num').val(),
                this.$el.find('.js-perm-rec').prop('checked')
            );
        },
        templateHelpers: function(){
            var files = this.options.model.get('files');
            return {
                'filesCaption': files.length > 2 ? files.slice(0,2).join(', ') + ' ...'  : files.join(', '), // Если файлов много показываем только 2
                'filesTitle': files.join(', ')                                                               // А остальные выводим в title
            }
        }

    });

    /**
     * Вьюшка чекбоксов для установки прав
     * @type {*}
     */
    var FilePermCheckboxesView = Backbone.Marionette.ItemView.extend({
        template: '#js-file-perm-checkboxes-tpl',
        events: {
            'change input.js-perm': 'change'
        },
        initialize: function(){
            // Числовые права изменились надо перерисовать чекбоксы
            this.model.on('file:perm:refresh', function(){
                this.render();
            }.bind(this));
        },
        /**
         * Состояние чекбоксов изменилось
         */
        change: function(ev){
            var checkbox = $(ev.target);
            this.model.set(checkbox.attr('name'), String(+checkbox.prop('checked')));
            this.model.trigger('refresh:num:perm');     // Событие на изменение числовых прав
        },
        onRender: function(){
            var that = this;
            // Проставляем каждому чекбоксу его значение
            this.$el.find('input.js-perm').each(function(){
                $(this).prop('checked', !!parseInt(that.model.get($(this).attr('name'))));  // Забавно но !!"0" не false
            });
        }
    });

    /**
     * Вьюшка ссылка на корень сайта
     * @type {*|void}
     */
    var DocRootsSiteView = Backbone.Marionette.ItemView.extend(commonVars).extend({
        tagName: 'li',
        template: '#js-docroot-site-tpl',
        events: {
            'click a': 'goRoot'
        },
        initialize: function(){
            this.model.on('change:active', function(){  // Ссылка стала активной или нет
                this.render();
            }.bind(this));
        },
        templateHelpers: function(){
            var caption = this.model.get('idn') ? this.model.get('idn') : this.model.get('name');
            return {
                link: this.docRootPath.replace('%s', this.model.get('name')),
                caption: caption,
                captionShort: caption.length > 27 ? caption.substr(0,24) + '...' : caption
            }
        },
        /**
         * Переходим на сайт
         * @param ev
         */
        goRoot: function(ev){
            ev.preventDefault();
            this.trigger('docroot:go:site', this.docRootPath.replace('%s', this.model.get('name')));
        }
    });

    /**
     * Вьюшка ссылок списков на сайты
     * @type {*}
     */
    var DocRootsView = Backbone.Marionette.CompositeView.extend({
        template: '#js-docroot-list-tpl',
        childView: DocRootsSiteView,
        childViewContainer: '.js-docroot-list'
    });

    /**
     * Отображение части пути
     * @type {*|void}
     */
    var PathView = Backbone.Marionette.ItemView.extend(commonVars).extend({
        tagName: 'span',
        template: '#js-path-tpl',
        events:{
            'click .js-path-link': 'pathClick'
        },
        /**
         * Переход по пути path
         * @param ev
         */
        pathClick: function(ev){
            ev.preventDefault();
            this.trigger('go:to:path', this.model.get('path'));
        },
        templateHelpers: function(){
            return {
                dirNoSlash: this.model.get('dir').split('/').map(_.escape).join('<span class="sep sep-in">/</span>')    // Нужно для "домика", там есть /
            }
        }
    })

    /**
     * Вьюшка путей-хлебных крошек
     * @type {*}
     */
    var PathListView = Backbone.Marionette.CompositeView.extend({
        template: '#js-path-list-tpl',
        childView: PathView,
        childViewContainer: '.js-path-list'
    });

///////////////////
//  Контроллер   //
///////////////////

    // Контроллер файлового менеджера
    var FileManagerController = Backbone.Marionette.Controller.extend(commonVars).extend({
        initialize: function(options){

            var defaultOptions = {
                fields: {
                    'name': 'Имя файла',
                    'size': 'Размер',
                    'perm': 'Права',
//                    ctime: 'Создан',
                    mtime: 'Изменен'
                },  // Поля таблицы по умолчанию
                showCheckbox: false,
                startPath: ''
            };

            options = _.extend(defaultOptions, options);


            var fieldsToShow = options.showCheckbox ? _.extend({'checked':''}, options['fields']) : options['fields'];     // Добавляем к полям для отображения чекбокс

            var region = new Backbone.Marionette.Region({                       // Основной регион отображения
                el: '.file-manager'
            });

            var layout = new  FileNavigationLayout();                           // Шаблон с областями фаил-менеджера

            var fileCollection = new FileCollection();                          // Коллекция файлов
            var jobCollection = new JobCollection();                            // Коллекция джобов
            var docRootsCollection = new DocRootsCollection();                      // Коллекция корневых директорий сайтов
            var pathCollection = new PathCollection();

            var blockView = new BlockView();                                     // Вьюшка блокировки контента при загрузке

            if(options.startPath){
                fileCollection.setPath(options.startPath);
            }



            // Сисок файлов успешно загружен
            fileCollection.on('reset', function(){

                if(fileCollection.isShowParent()){
                    // Добавляем переход в родителя
                    // @todo надо это переделать, убрать в коллекцию ???
                    var goParent = {};
                    for (var f in fieldsToShow){
                        goParent[f] = (f != 'name') ? '' : '..';
                    }
                    goParent['type'] = 'dir';
                    goParent['isParent'] = true;
                    fileCollection.add( goParent , {at: 0})
                }

                // Тело таблицы файлов
                var fileNavigationListView = new FileNavigationListView({
                    collection: fileCollection,
                    showFields: fieldsToShow
                });

                // Шапка таблицы файлов
                var fileNavigationHeadView = new FileNavigationHeadView({
                    showFields: fieldsToShow
                });

                pathCollection.list(fileCollection.getCurrntPath());

                layout.head.show(fileNavigationHeadView);
                layout.body.show(fileNavigationListView);

                // Мы пытаемся открыть слишком большой фаил
                this.listenTo(fileNavigationListView,
                    'childview:childview:file:big:size:error',
                    function(){
                        this.errorDialog('#js-big-file-ui-error-tpl');
                    }.bind(this));

                // Нажатие кнопки копирования
                this.listenTo(
                    layout,
                    'copy:init', function(){
                        this.copyInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки перемещения
                this.listenTo(
                    layout,
                    'move:init', function(){
                        this.moveInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки переименования
                this.listenTo(
                    layout,
                    'rename:init', function(){
                        this.renameInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки загрузки файла
                this.listenTo(
                    layout,
                    'upload:init', function(){
                        this.uploadInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки удаления файлов
                this.listenTo(
                    layout,
                    'delete:init', function(){
                        this.deleteInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки создания директории
                this.listenTo(
                    layout,
                    'create:dir:init', function(){
                        this.createDirInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки создания файла
                this.listenTo(
                    layout,
                    'create:file:init', function(){
                        this.createFileInit(fileCollection)
                    }.bind(this));

                // Нажатие кнопки изменения прав файла
                this.listenTo(
                    layout,
                    'perm:init', function(){
                        this.permInit(fileCollection)
                    }.bind(this));

                ///////////
                // Контекстное меню
                ///////////

                // Выбрано много файлов
                this.listenTo(
                    fileNavigationListView,
                    'file:many:selected', function(){
                        layout.contextForMany();
                    }.bind(this));

                // Не выбрано ничего
                this.listenTo(
                    fileNavigationListView,
                    'file:empty:selected', function(){
                        layout.contextForEmpty();
                    }.bind(this));

                // Выбран архив
                this.listenTo(
                    fileNavigationListView,
                    'file:arc:selected', function(){
                        layout.contextForArc();
                    }.bind(this));

                // Выбран не архив
                this.listenTo(
                    fileNavigationListView,
                    'file:image:selected file:text:selected file:dir:selected file:file:selected ', function(){
                        layout.contextForOne();
                    }.bind(this));

            }.bind(this));


            //  Список джобов успешно загружен
            jobCollection.on('reset', function(){
                // Если работы есть
                if(jobCollection.length){
                    // Джоб получен, теперь хотим получить по нему статус
                    jobCollection.getStatus();

                }
            }.bind(this));

            var jobListView ;
            // Статус джоба успешно загружен
            jobCollection.on('job:status:loaded', function(){
                // @todo Запутанное обновление

                /**
                 * Функция установки интервала обновления работ
                 * @type {function(this:FileManagerController)|*}
                 */
                var setJobInterval = function(){
                    this.jobInterval = setInterval(function(){
                        jobCollection.getStatus();
                    }, this.jobRefreshTime)             // Обновляем работы с этим интервалом
                }.bind(this);

                if(!jobListView){
                    // Если вьюшка списка джобов ещё не создана
                    jobListView = new JobListView({
                        collection: jobCollection
                    });

                    layout.jobs.show(jobListView);

                    // Событие удаления работы
                    jobCollection.listenTo(jobListView, 'childview:job:delete', function(view){
                        jobCollection.delete(view.model);
                    }.bind(this));

                    // Событие отмены работы
                    jobCollection.listenTo(jobListView, 'childview:job:cancel', function(view){
                        jobCollection.cancel(view.model);
                    }.bind(this));

                    setJobInterval();   // Запускаем обновление

                } else {
                    // Если у нас уже есть вьюшка листа
                    jobListView.children.each(
                        function(view){
                            // Обновляем каждый элемент в листе (один элемент)
                            view.trigger('job:refresh')
                        });


                }

                // Магия отмены обновления работ
                if(!jobCollection.length ||                     // Если список работ пустой
                    jobCollection.at(0).get('error') ||         // Если у нашей единственной работы статус ошибка
                    jobCollection.at(0).get('calcStep') == jobCollection.at(0).get('calcStepLength') - 1    // Если мы пришли на последний шаг
                    ){
                    if(this.jobInterval){
                        clearInterval(this.jobInterval);    // Удаляем интервал
                        delete this.jobInterval;            // Удаляем номер интервала
                    };
                    jobCollection.each(function(model){ // Удаляем все работы в листе (одну)
                        jobCollection.delete(model);
                    }.bind(this));
                    layout.disableCopy(0);  // Разблокируем кнопку копирования
                } else {
                    !this.jobInterval && setJobInterval();      // Если вдруг работы не обновляются
                }

            }.bind(this));

            // Список сайтов успешно получен
            docRootsCollection.on('reset', function(){
                var docRootsView = new DocRootsView({
                    collection: docRootsCollection
                });

                docRootsView.on('childview:docroot:go:site', function(view, link){
                    fileCollection.setPath(link);
                    fileCollection.list();
                }.bind(this))

                layout.docroots.show(docRootsView);
            });

            // Список путей "хлебных крошек"
            pathCollection.on('reset', function(){
                var pathListView = new PathListView({
                    collection: pathCollection
                });

                // Событие перехода в другую директорию
                pathListView.on('childview:go:to:path', function(view, path){
                    fileCollection.setPath(path);                               // Усанавливаем путь
                    fileCollection.list();                                      // Обновляем коллекцию файлов
                });

                layout.path.show(pathListView);                                 // Отображаем вьюшку с "хлебными крошками"
            })

            // Событие того, что текущая директория содержит в себе путь к корню сайта пользователя если site не пустой, или наоборот, тогда site == ''
            fileCollection.on('file:in:site:dir', function(site){
                docRootsCollection.setActive(site);
            })

            //  Событие удаления и отмены работы
            jobCollection.on('job:deleted job:canceled', function(){
                // Разблокируем кнопку копирования
                layout.disableCopy(0);
            });

            // Дисаблим кнопку копирования при непустом списке джобов
            layout.listenTo(jobCollection, 'job:list:loaded', layout.disableCopy.bind(layout));

            // Событие перехода по папкам
            // @todo убрать в коллекцию
            this.listenTo(fileCollection, 'change:path', function(dir){
                fileCollection.setDir(dir);
                fileCollection.list();
            });

            // Событие открытия файла на редактирование
            this.listenTo(fileCollection, 'file:edit:ready', function(fileEditModel){
                this.showEditorDialog(fileEditModel);

            }.bind(this));

            // Событие обновления списка файлов
            this.listenTo(this, 'file:list:refresh', function(){
                fileCollection.refresh();
            }.bind(this));

            // Задание на копирование успешно отправлено
            this.listenTo(fileCollection, 'file:copy:staged', function(){
                jobCollection.refresh();
            }.bind(this));

            // Ошибка перемещения
            this.listenTo(fileCollection, 'file:move:error', function(errors){
                this.errorOutputDialog('Невозможно переместить некоторые файлы:', errors.join("\n"));
            }.bind(this));

            // Ошибка копирования
            this.listenTo(jobCollection, 'file:copy:error', function(errors){
                this.errorOutputDialog('При копировании возникли ошибки:', errors.join("\n"));
            }.bind(this));

            // Ошибка удаления
            this.listenTo(fileCollection, 'file:delete:error', function(errors){
                this.errorOutputDialog('Невозможно удалить некоторые файлы:', errors.join("\n"));
            }.bind(this));

            // Ошибка применения прав
            this.listenTo(fileCollection, 'file:perm:error', function(errors){
                this.errorOutputDialog('При изменении прав возникли ошибки:', errors.join("\n"));
            }.bind(this));

            // Фатальная ошибка при работе с фалами
            this.listenTo(
                fileCollection,
                'file:list:fail file:copy:fail file:move:fail file:delete:fail file:view:text:fail file:save:text:fail',
                function(){
                    this.errorDialog('#js-common-ui-error');
                }.bind(this)
            );

            // Фатальная ошибка при работе с джобами
            this.listenTo(
                jobCollection,
                'job:list:fail job:delete:fail job:cancel:fail',
                function(){
                    this.errorDialog('#js-common-ui-error');
                }.bind(this)
            );

            // Показываем блокирующую контент крутилку
            this.listenTo(
                fileCollection,
                'file:list:before file:delete:before',
                function(){
                    blockView.showBlock();
                }
            );

            // Скрываем блокирующую контент крутилку
            this.listenTo(
                fileCollection,
                'file:list:fail file:list:success',
                function(){
                    blockView.hideBlock();
                }
            );

            // Перед началом загрузки файл блокируем область контента
            this.on(
                'file:upload',
                function(){
                    blockView.showBlock();
                }
            );

            // Если загрузка файла не удалась
            this.on(
                'file:upload:fail',
                function(){
                    blockView.hideBlock();
                    this.errorDialog('#js-upload-ui-error');
                }.bind(this)
            );

            // Если загрузка файла не удалась по причине большого размера файла
            this.on(
                'file:upload:maxsize:fail',
                function(){
                    blockView.hideBlock();
                    this.errorDialog('#js-upload-maxsize-ui-error');
                }.bind(this)
            );

            // Событие создания директории
            this.on('dir:create', function(name){
                fileCollection.createFile(name, 'dir');
            }.bind(this));

            this.on('file:save:editor', function(model){
                fileCollection.saveText(model);
            });

            // Событие переименования файла
            this.on('file:rename', function(name){
                fileCollection.rename(name);
            });

            region.show(layout);
            layout.block.show(blockView);
            layout.contextForEmpty();
            fileCollection.list();  // Запрос на получение файлов
            jobCollection.list();   // Запрос на получение работ
            docRootsCollection.list() // Получение корневых директорий сайтов
            pathCollection.list(fileCollection.getCurrntPath()) // Получение путей в хлебные крошки
        },

        /**
         * Подготовка к копированию
         * Мы показываем диалог с деревом папок
         *
         * @param fileCollection // Экземпляр FileCollection, она знает имена всех файлов и путь к текущей директории
         */
        copyInit: function(fileCollection){
            var fileNames = fileCollection.getCheckedNames(); // массив имен файлов
            if(!fileNames.length){
                //Ни одного файла не выбрано
                this.errorDialog('#js-no-file-ui-error-tpl');
                return;
            }

            this.showTreeDialog("#js-copy-ui-dialog-tpl", '#js-copy-ui-dialog-region', fileCollection.copy.bind(fileCollection));

        },

        /**
         * Подготовка к перемещению
         * @param fileCollection
         */
        moveInit: function(fileCollection){
            // @todo дублирование
            var fileNames = fileCollection.getCheckedNames(); // массив имен файлов
            if(!fileNames.length){
                this.errorDialog('#js-no-file-ui-error-tpl');
                return;
            }

            this.showTreeDialog("#js-move-ui-dialog-tpl", '#js-move-ui-dialog-region', fileCollection.move.bind(fileCollection));

        },

        /**
         * Подготовка к переименованию
         * @param fileCollection
         */
        renameInit: function(fileCollection){
            var fileNames = fileCollection.getCheckedNames(); // массив имен файлов
            if(!fileNames.length){                              // Должен быть выбран только один элемент
                this.errorDialog('#js-no-file-ui-error-tpl');
                return;
            } else if(fileNames.length > 1){
                this.errorDialog('#js-many-file-ui-error-tpl');
                return;
            }

            var that = this;
            var uiDialog = $('#js-file-rename-ui-tpl');
            // Защита от двойного присвоения событий jquery ui диалога
            if(!uiDialog.hasClass('ui-dialog-content')) {
                uiDialog.dialog({
                    modal: true,
                    width: 300,
                    create: function( event, ui ) {
                        $('form', this).bootstrapValidator({        // Валидируем форму
                            fields: {
                                fileName: {
                                    validators: {
                                        notEmpty: {
                                            message: 'Имя не может быть пустым!'
                                        }
                                    }
                                }
                            }
                        }).on('success.form.bv', function(ev){                              // Форма успешно прошла валидацию
                                ev.preventDefault();
                                that.trigger('file:rename', $('.js-file-name', this).val());
                                $(this).dialog( "close" );
                            }.bind(this));

                        $('.js-cancel', this).button().on('click', function(){
                            $(this).dialog("close");
                        }.bind(this));

                        $('.js-save', this).button();
                    },
                    beforeClose: function( event, ui ) {
                        $(this).data().uiDialog.opener = $('div');
                    }
                });
            }

            uiDialog.find('.js-file-name').val(fileNames[0]);   // У нас всего одно имя, устанавливаем его в инпут
            uiDialog.dialog("open");

        },

        /**
         * Подготовка к удалению
         * @param fileCollection
         */
        deleteInit: function(fileCollection){
            var fileNames = fileCollection.getCheckedNames(); // массив имен файлов
            if(!fileNames.length){
                //Ни одного файла не выбрано
                this.errorDialog('#js-no-file-ui-error-tpl');
                return;
            }
            var uiDialog = $('#js-delete-ui-dialog-tpl');
            uiDialog.dialog({
                modal: true,
                buttons: [
                    {
                        text: "Удалить",
                        click: function() {
                            fileCollection.delete();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Отменить",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ],
                /**
                 * Отменяем фокусировку кнопок после после закрытия диалога
                 * @param event
                 * @param ui
                 */
                beforeClose: function( event, ui ) {
                    $(this).data().uiDialog.opener = $('div');
                },
                close: function( event, ui ) {

                },
                open: function( event, ui ) {
                }
            });
            uiDialog.dialog("open");
        },

        /**
         * Окно загрузки файлов
         */
        uploadInit: function(fileCollection){
            var that = this;
            var uiDialog = $('#js-upload-ui-dialog-tpl');
            uiDialog.dialog({
                modal: true,
                height:150,
                buttons : {
                    "Загрузить": function() {
                        $(this).dialog("close");
                        that.trigger('file:upload');    // Показываем крутирлку
                        // ajax загрузка файлов
                        $(this).find('form').ajaxSubmit({
                            data:{
                                path: fileCollection.currentPath,
                                action: 'upload'
                            },
                            success: function(){
                                fileCollection.refresh(); // Обновляем коллекцию, крутилку не скрываем
                            }.bind(this),
                            error: function(xhr){
                                if(xhr.status == 413){              // Если превышен максимальный размер загружаемого файла
                                    that.trigger('file:upload:maxsize:fail');
                                } else {                            // Остальные ошибки
                                    that.trigger('file:upload:fail');
                                }
                            }.bind(this)

                        }); // загружаем фаил
                    },
                    "Отменить": function() {
                        $(this).dialog("close");
                    }

                },
                beforeClose: function( event, ui ) {
                    $(this).data().uiDialog.opener = $('div');
                },
                close: function( event, ui ) {

                },
                open: function( event, ui ) {
                    $(this).find('form').trigger("reset");  // Сбрасываем поля формы
                }

            });
            uiDialog.dialog("open");
        },

        /**
         * Диалог создания новой директории
         * @param fileCollection
         */
        createDirInit: function(fileCollection){
            var that = this;
            var uiDialog = $('#js-create-dir-tpl');
            // Защита от двойного присвоения событий jquery ui диалога
            if(!uiDialog.hasClass('ui-dialog-content')) {
                uiDialog.dialog({
                    modal: true,
                    width: 300,
                    create: function( event, ui ) {
                        $('form', this).bootstrapValidator({
                            fields: {
                                fileName: {
                                    validators: {
                                        notEmpty: {
                                            message: 'Имя не может быть пустым!'
                                        },
                                        regexp: {
                                            regexp: /^[^а-яА-ЯёЁ]+$/i,
                                            message: 'Недопустимое имя. В наименовании директорий рекомендуем использовать латинские символы и цифры.'
                                        }
                                    }
                                }
                            }
                        }).on('success.form.bv', function(ev){                              // Форма успешно прошла валидацию
                                ev.preventDefault();
                                that.trigger('dir:create', $('.js-file-name', this).val());
                                $(this).dialog( "close" );
                            }.bind(this));

                        $('.js-cancel', this).button().on('click.createDir', function(){
                            $(this).dialog(  "close"  );
                        }.bind(this));

                        $('.js-save', this).button();
                    },
                    open: function( event, ui ) {
                        $('.js-file-name', this).val('');
                    },
                    beforeClose: function( event, ui ) {
                        $(this).data().uiDialog.opener = $('div');
                    },
                    close: function( event, ui ) {

                    }
                });
            }

            uiDialog.dialog("open");
        },

        /**
         * Создание нового файла
         * @param fileCollection
         */
        createFileInit: function(fileCollection){
            // Создаем модель редактирования файла
            var fileEditModel = new FileEditModel({
                'curPath': fileCollection.getCurrntPath()
            })
            this.showEditorDialog(fileEditModel);

        },

        /**
         * Диалог изменения прав
         * @param fileCollection
         */
        permInit: function(fileCollection){
            var fileNames = fileCollection.getCheckedNames(); // массив имен файлов
            if(!fileNames.length){
                //Ни одного файла не выбрано
                this.errorDialog('#js-no-file-ui-error-tpl');
                return;
            }

            var uiDialog = $('#js-perm-ui-dialog-tpl');
            var that = this;
            uiDialog.dialog({
                modal: true,
                resizable: false,
                width: 282,
                open: function( event, ui ) {
                    this.permRegion = that.showPermForm(
                        fileCollection,
                        function(){
                            $(this).dialog('close');
                        }.bind(this)
                    );
                    $(this).parent().css('overflow', 'hidden');
                },
                close: function(){
                    if(this.permRegion){
                        this.permRegion.reset();
                    }
                }
            });
            uiDialog.dialog("open");
        },

        /**
         * Показываем диалог с деревом
         * Он практически одинаков для копирования и перемещения
         * @param uiTemplate // Шаблон диалога
         * @param treeRegion // селектор региона в диалоге
         * @param onSelect // Событие при выборе
         */
        showTreeDialog: function(uiTemplate, treeRegion, onSelect){
            var uiDialog = $(uiTemplate);
            var that = this;
            uiDialog.dialog({
                modal: true,
                resizable: true,
                width:'auto',
                buttons : {
                    "Выбрать": function() {
                        onSelect($(this).find('.js-tree-node.active').data('path'));  // Просто ищем ссылку с классом active
                        $(this).dialog("close");
                    },
                    "Отменить": function() {
                        $(this).dialog("close");
                    }

                },
                beforeClose: function( event, ui ) {
                    $(this).data().uiDialog.opener = $('div');
                },
                close: function( event, ui ) {
                    this.tree && this.tree.remove();                  // Уничтожаем вьюшку дерева при выходе
                },
                open: function( event, ui ) {
                    $(this).parent().addClass('js-click-in');
                    this.tree = that.showTree(treeRegion); // Сохраняем в ссылку на вьюху @todo надо додумать как средстваим backbone предавать путь в "Выбор"
                }

            });
            uiDialog.dialog("open");
        },

        /**
         * Алерт с ошибкой.
         * @param uiTemplate // Шаблон jquery-ui диалога ошибки
         * @param onClose
         */
        errorDialog: function(uiTemplate, onClose){
            var uiDialog = $(uiTemplate);
            uiDialog.dialog({
                modal: true,
                buttons : {
                    "Ок": function() {
                        $(this).dialog("close");
                    }
                },
                open: function( event, ui ) {
                    $(this).parent().addClass('js-click-in');
                },
                beforeClose: function( event, ui ) {
                    $(this).data().uiDialog.opener = $('div');
                },
                close: function( event, ui ) {
                    _.isFunction(onClose) && onClose();
                }
            });
            uiDialog.dialog("open");
        },

        /**
         * Окно с описанием ошибки
         * @param description // Описание операции
         * @param output      // Содержание ошибки
         */
        errorOutputDialog: function(description, output){
            var uiDialog = $('#js-file-output-ui-error-tpl');
            uiDialog.dialog({
                modal: true,
                buttons : {
                    "Ок": function() {
                        $(this).dialog("close");
                    }
                },
                open: function( event, ui ) {
                    $('.js-description', this).text(description);
                    $('.js-output', this).text(output);
                }
            });
            uiDialog.dialog("open");
        },

        /**
         * Отображение дерева
         * @param treeRegion // Селектор региона где отображать дерево
         * @returns {FileTreeView}
         */
        showTree: function(treeRegion){
            var regionTree = new Backbone.Marionette.Region({
                el: treeRegion    // @todo надо передавать параметром
            });

            var rootNode = new FileTreeNode({
                name: '/',
                with_subdirs: true,
                isOpen: true
            });
            rootNode.loadNode('/');
            regionTree.$el.next('.js-block-dialog-region').show();  // Показываем загрузчик @todo переделать на events & layout
            var fileTreeView = new FileTreeView({
                model: rootNode
            });
            rootNode.on('tree:file:loaded', function(){
                regionTree.$el.next('.js-block-dialog-region').hide();  // Скрываем загрузчик @todo переделать на events & layout
                regionTree.show(fileTreeView);
            })

            return fileTreeView;
        },

        /**
         * Показываем диалог с редактором
         * @param fileEditModel
         */
        showEditorDialog: function(fileEditModel){
            var that = this;
            var uiDialog = $('#js-edit-ui-dialog-tpl');
            uiDialog.dialog({
                modal: true,
                position: {
                    my: 'center top+10%',
                    at: 'center top',
                    of: $('.file-manager')
                },
                minWidth: $(window).width() * 0.9,
                minHeight:$(window).height() * 0.9,
                close: function( event, ui ) {

                },
                open: function( event, ui ) {
                    that.showEditor(
                        fileEditModel,
                        function(){
                            $(this).dialog('close');
                        }.bind(this)
                    );
                    if(fileEditModel.get('error')){
                        $(this).parent().find('.js-save').attr('disabled', 'disabled');
                    }
                }

            });
            uiDialog.dialog("open");
        },

        /**
         * Показываем редактор
         * @param fileEditModel
         */
        showEditor: function(fileEditModel, onCloseEditor){
            var editorRegion = new Backbone.Marionette.Region({
                el: '#js-file-manager-editor-region'
            });

            var fileEditorView = new FileEditorView({
                model: fileEditModel,
                onCloseEditor: onCloseEditor            // Функция ссылка на закрытие окна диалога
            });

            this.listenTo(fileEditorView, 'file:save:editor', function(model){
                this.trigger('file:save:editor', model );                       // Пробрасываем событие сохранения дальше
            }.bind(this));

            this.listenTo(fileEditorView, 'editor:destroy', function(){
                editorRegion.reset();                               // Уничтожаем регион с редактором
            });

            editorRegion.show(fileEditorView);
        },

        showPermForm:function(fileCollection, onCloseEditor){
            var permRegion = new Backbone.Marionette.Region({
                el: '#js-file-perm-region'
            });

            var fileModel = fileCollection.where({checked: true})[0];                   // Берем права первого попавшегося файла

            var fileNames = fileCollection.where({checked: true}).map(function(model){  // Массив имен файлов
                return model.get('name');
            });

            var filePermModel = new FilePermModel(                                      // Модель прав
                {
                    files: fileNames,
                    perm: fileModel.get('perm')
                });

            var filePermView = new FilePermView({                                           // Вьюшка формы прав
                model: filePermModel
            });

            var filePermCheckboxesView = new FilePermCheckboxesView({                   // Вьюшка чекбоксов в форме
                model: filePermModel
            });

            this.listenTo(filePermView, 'file:perm:close', function(){                  // Закрытие окна прав
                onCloseEditor();
            });

            this.listenTo(filePermView, 'file:perm:num:change', function(val){          // Событие измения числовых прав в форме
                filePermModel.set('perm', val);                                         // Изменяем модель прав
            });

            /**
             * По измению модели чекбоксов устанавливаем числовые права
             */
            this.listenTo(filePermModel, 'change:num:perm', function(perm){
                filePermView.setNumPerm(perm);
            });

            this.listenTo(filePermView, 'file:perm:save', function(perm, recursion){    // Сохранение прав
                fileCollection.changePerm(perm, recursion);
                onCloseEditor();
            });

            permRegion.show(filePermView);                                              // Показ формы прав в регионе
            filePermView.permCheckboxes.show(filePermCheckboxesView);                   // Показ чекбоксов в форме
        }

    });

    $(function(){
        new FileManagerController();
    });

})(jQuery);